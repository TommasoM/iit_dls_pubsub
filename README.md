# Assignement for the Dinamic Legged Systems technical interview.

## -- Assignement

The goal of this assessment is to better understand your abilities. In particular we will be looking at:
- problem solving ability
- code documentation
- test coverage
- time management
- OOP
- learning a new library

To solve the problem in its entirety is impossible within the time frame given.  The deadline to finish is on by the end of Tuesday the 23rd of February.  We assume that you will spend 1 working day (8 hours) in total on it.  Please keep track of your time spent and include a short rundown of where you spent your time.  We typically give a few more days to work on the project (still 8 hours of actual work) as everybody has busy schedules, but we would like to finish the hiring process as soon as possible.  Let me know if you will need extra time.

The problem: Design a publisher/subscriber library using Fast DDS.
https://github.com/eProsima/Fast-DDS
The online documentation provides a simple publisher / subscriber application that can be used as a starting point.
Tasks:
- Create custom messages
- Stream messages between separate processes
- Send a single message / response between separate processes
- Display published messages in the terminal
- Display what processes are publishing messages
- Stress test system
- Measure rate of sent messages
- Measure latency of sent messages

## -- Instructions --

To run the project, first you have to setup the environment:

https://fast-dds.docs.eprosima.com/en/latest/installation/sources/sources_linux.html

Then, go into the root directory of this project and execute the following commands:

	mkdir build && cd build
	cmake ..
	make clean && make

Then start publisher and subscriber tasks on two different terminal

T1:
	./DDSHelloWorldPublisher

T2:
	./DDSHelloWorldSubscriber

## -- Log --

### Saturday 2021/02/20, 17:00

I had some problem trying to configure the system because of three main issues:
- Some dependencies missing after installation process
- Almost any guide at some point require to use the tool
	<path/to/Fast DDS-Gen>/scripts/fastrtpsgen HelloWorld.idl 
	This tool doesn't exists anymore!
- The github link for the ShapesDemo on the documentation is wrong
	https://raw.githubusercontent.com/eProsima/ShapesDemo/master/ShapesDemo.repos doesn't exists, it's
	https://raw.githubusercontent.com/eProsima/ShapesDemo/master/shapes-demo.repos works
	I made this demo work then but I wanted something easier to start developing the requested tool.

Because this VM may have some problem with dependencies and installation, I decided to start again the next day with a new installation from scratches.

### 19:00 stop

### Sunday 2021/02/21 11:00	
New dev VM confiugration	
Xubuntu 20.04 download and install
Fast-DDS install

Installing a new VM with Xubuntu 20.04
tom@ubuntu:~/Desktop/IITassignement$ sudo apt install default-jdk
tom@ubuntu:~/Desktop/IITassignement$ sudo apt install gradle

Installation from source
Dependencies:
tom@ubuntu:~/Desktop/IITassignement$ sudo apt install libasio-dev libtinyxml2-dev

Colcon installation
tom@ubuntu:~/Desktop/IITassignement$ sudo apt install python3-pip
tom@ubuntu:~/Desktop/IITassignement$ sudo pip3 install -U colcon-common-extensions vcstool

Download the repos file that will be used to download Fast RTPS and its dependencies
tom@ubuntu:~/Desktop/IITassignement$ mkdir fastdds_ws && cd fastdds_ws
tom@ubuntu:~/Desktop/IITassignement/fastdds_ws$ wget https://raw.githubusercontent.com/eProsima/Fast-DDS/master/fastrtps.repos
tom@ubuntu:~/Desktop/IITassignement/fastdds_ws$ mkdir src
tom@ubuntu:~/Desktop/IITassignement/fastdds_ws$ sudo vcs import src < fastrtps.repos
>
Gets errors on git, need to install it
tom@ubuntu:~/Desktop/IITassignement/fastdds_ws$ sudo apt upgrade
tom@ubuntu:~/Desktop/IITassignement/fastdds_ws$ sudo apt install git

tom@ubuntu:~/Desktop/IITassignement/fastdds_ws$ sudo vcs import src < fastrtps.repos
>
It should have installed also Fast-DDS but I had some trouble on a previous try, let's go for the manual installation of Fast-DDS
tom@ubuntu:~/Desktop/IITassignement/fastdds_ws$ git clone https://github.com/eProsima/Fast-DDS.git
tom@ubuntu:~/Desktop/IITassignement/fastdds_ws$ mkdir Fast-DDS/build && cd Fast-DDS/build
tom@ubuntu:~/Desktop/IITassignement/fastdds_ws/Fast-DDS/build$ sudo apt install cmake
tom@ubuntu:~/Desktop/IITassignement/fastdds_ws/Fast-DDS/build$ cmake ..

It cannot find Fast RTPS, it looks like the colcon installation gone wrong, but it didn't display any error message. Let's go for the whole manual installation.
tom@ubuntu:~/Desktop/IITassignement$ rm -rf fastdds_ws
tom@ubuntu:~/Desktop/IITassignement$ git clone https://github.com/eProsima/Fast-CDR.git
tom@ubuntu:~/Desktop/IITassignement$ mkdir Fast-CDR/build && cd Fast-CDR/build
tom@ubuntu:~/Desktop/IITassignement/Fast-CDR/build$ sudo cmake ..
tom@ubuntu:~/Desktop/IITassignement/Fast-CDR/build$ sudo cmake --build . --target install
tom@ubuntu:~/Desktop/IITassignement$ git clone https://github.com/eProsima/foonathan_memory_vendor.git
tom@ubuntu:~/Desktop/IITassignement$ cd foonathan_memory_vendor
tom@ubuntu:~/Desktop/IITassignement/foonathan_memory_vendor$ mkdir build && cd build
tom@ubuntu:~/Desktop/IITassignement/foonathan_memory_vendor/build$ sudo cmake ..
tom@ubuntu:~/Desktop/IITassignement/foonathan_memory_vendor/build$ sudo cmake --build . --target install
tom@ubuntu:~/Desktop/IITassignement$ git clone https://github.com/eProsima/Fast-DDS.git
tom@ubuntu:~/Desktop/IITassignement$ mkdir Fast-DDS/build && cd Fast-DDS/build
tom@ubuntu:~/Desktop/IITassignement/Fast-DDS/build$ sudo cmake ..
tom@ubuntu:~/Desktop/IITassignement/Fast-DDS/build$ sudo cmake --build . --target install

### 12:00 Stop and I let cmake build and install

### Sunday 2021/02/21, 15:30

tom@ubuntu:~/Desktop/IITassignement$ sudo apt-get install docker.io


Because of the problems I had with this installation the day before, I decided to do again the installation following the installation manual instructions, which are slightly different from the instructions on the github page.
cd ~/Fast-DDS
git clone https://github.com/eProsima/foonathan_memory_vendor.git
mkdir foonathan_memory_vendor/build
cd foonathan_memory_vendor/build
cmake .. -DCMAKE_INSTALL_PREFIX=~/Fast-DDS/install -DBUILD_SHARED_LIBS=ON
sudo cmake --build . --target install
cd ~/Fast-DDS
git clone https://github.com/eProsima/Fast-CDR.git
mkdir Fast-CDR/build
cd Fast-CDR/build
cmake .. -DCMAKE_INSTALL_PREFIX=~/Fast-DDS/install
sudo cmake --build . --target install
cd ~/Fast-DDS
git clone https://github.com/eProsima/Fast-DDS.git
mkdir Fast-DDS/build
cd Fast-DDS/build
cmake ..  -DCMAKE_INSTALL_PREFIX=~/Fast-DDS/install -DCMAKE_PREFIX_PATH=~/Fast-DDS/install
sudo cmake --build . --target install

export LD_LIBRARY_PATH=/usr/local/lib/
echo 'export LD_LIBRARY_PATH=/usr/local/lib/' >> ~/.bashrc

cd ~
git clone --recursive https://github.com/eProsima/Fast-DDS-Gen.git
cd Fast-DDS-Gen
gradle assemble

Again problem with gradle... Let's try with another gradle version, the apt one is probably outdated
tom@ubuntu:~/Fast-DDS-Gen$ sudo apt remove gradle
tom@ubuntu:~/Fast-DDS-Gen$ sudo apt install curl
tom@ubuntu:~/Fast-DDS-Gen$ sudo curl -s "https://get.sdkman.io" | bash
tom@ubuntu:~/Fast-DDS-Gen$ sudo curl -s "https://get.sdkman.io" | bash
tom@ubuntu:~/Fast-DDS-Gen$ source "$HOME/.sdkman/bin/sdkman-init.sh"
tom@ubuntu:~/Fast-DDS-Gen$ sdk install gradle 6.8.2
tom@ubuntu:~/Fast-DDS-Gen$ gradle assemble

Now gradle it's working, but I still can't find the script fastrtpsgen... I did the installation process three times (including Saturday) because of the outdated gradle version on apt.

Trying the installation from binaries
downloading and extracting eProsima_Fast_DDS-2.2.0-Linux.tgz

tom@ubuntu:~/Desktop/IITassignement/binaries$ sudo ./install.sh

Still the same problem, cannot find the script.

Changing guide: https://fast-dds.docs.eprosima.com/en/latest/fastddsgen/pubsub_app/pubsub_app.html

Ok previous steps, then 3.6.1. Generate the Fast DDS source code

tom@ubuntu:~/Desktop/IITassignement/FastDDSGenHelloWorld$ ~/Desktop/IITassignement/eProsima_Fast_DDS/src/fastrtpsgen/scripts/fastddsgen -example CMake HelloWorld.idl
tom@ubuntu:~/Desktop/IITassignement/FastDDSGenHelloWorld/build$ sudo cmake ..
tom@ubuntu:~/Desktop/IITassignement/FastDDSGenHelloWorld/build$ sudo make

With 2 different terminals
tom@ubuntu:~/Desktop/IITassignement/FastDDSGenHelloWorld/build$ ./HelloWorld publisher
tom@ubuntu:~/Desktop/IITassignement/FastDDSGenHelloWorld/build$ ./HelloWorld subscriber

It Works !!
But I found what seems to be an even more detailed guide and a better starting point:
https://fast-dds.docs.eprosima.com/en/latest/fastdds/getting_started/simple_app/simple_app.html

tom@ubuntu:~/Desktop/IITassignement/simpleApp$ mkdir workspace_DDSHelloWorld && cd workspace_DDSHelloWorld
tom@ubuntu:~/Desktop/IITassignement/simpleApp/workspace_DDSHelloWorld$ mkdir src build

Create the CMakeLists.txt file
Create the src/HelloWorld.idl

tom@ubuntu:~/Desktop/IITassignement/workspace_DDSHelloWorld/src$ ~/Desktop/IITassignement/eProsima_Fast_DDS/src/fastrtpsgen/scripts/fastddsgen -example CMake HelloWorld.idl

I used the fastddsgen script even if on the guide it says to use
<path/to/Fast DDS-Gen>/scripts/fastrtpsgen HelloWorld.idl
Even on git I cannot find this script but just some reference to it in the comments. It looks fastddsgen did the same work, let's see if it gives some problems in the future.

tom@ubuntu:~/Desktop/IITassignement/workspace_DDSHelloWorld/src$ wget -O HelloWorldPublisher.cpp \
https://raw.githubusercontent.com/eProsima/Fast-RTPS-docs/master/code/Examples/C++/DDSHelloWorld/src/HelloWorldPublisher.cpp

tom@ubuntu:~/Desktop/IITassignement/workspace_DDSHelloWorld/src$ wget -O HelloWorldSubscriber.cpp \
    https://raw.githubusercontent.com/eProsima/Fast-RTPS-docs/master/code/Examples/C++/DDSHelloWorld/src/HelloWorldSubscriber.cpp

CMakeLists.txt modified according to the guide

Next steps:
https://fast-dds.docs.eprosima.com/en/latest/fastdds/getting_started/simple_app/simple_app.html
1.3.7.1. Examining the code

### Stop 18:30 - 6 hours in

### Monday 2021/02/22, 7:30

Examining the code...

I copied the workspace_DDSHelloWorld as iit_pub_sub in order to be able to restart from a safe point if something goes wrong.

tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub/build$ cmake ..
tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub/build$ make clean && make

make error: "HelloWorldPubSubMain.cxx:(.text+0x0): multiple definition of `main';"
Let's try again...

tom@ubuntu:~/Desktop/IITassignement$ mkdir iit_pub_sub && cd iit_pub_sub
tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub$ mkdir src build

Create CMakeLists.txt
Create src/HelloWorld.idl

tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub/src$ ~/Desktop/IITassignement/eProsima_Fast_DDS/src/fastrtpsgen/scripts/fastddsgen -example CMake HelloWorld.idl
As I said earlier, this is different from the official guide, which asks to use fastrtpsgen, but this script doesn't exists!
According to the guide, this script generates more files then fastrtpsgen, later I'll try to remove the extra ones.

tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub/src$ wget -O HelloWorldPublisher.cpp https://raw.githubusercontent.com/eProsima/Fast-RTPS-docs/master/code/Examples/C++/DDSHelloWorld/src/HelloWorldPublisher.cpp

tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub/src$ wget -O HelloWorldSubscriber.cpp https://raw.githubusercontent.com/eProsima/Fast-RTPS-docs/master/code/Examples/C++/DDSHelloWorld/src/HelloWorldSubscriber.cpp

CMakeLists.txt modified according to the guide.

tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub/build$ cmake ..
tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub/build$ make clean && make

Still the same error for multiple main, let's try to remove the extra files.
It looks like the new script (fastddsgen) also does the cmake by itself...

tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub/build$ cmake ..
tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub/build$ make clean && make

Now it works!

Running from two different terminals
T1:
tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub/build$ ./DDSHelloWorldPublisher
T2:
tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub/build$ ./DDSHelloWorldSubscriber

It publish as soon as the subscriber connects, one message per second for 10 seconds, then both apps close.

Let's use this as a safe starting point, I create a copy as a backup.

<path-to-Fast-DDS-workspace> = ~/Desktop/IITassignement/eProsima_Fast_DDS/

### 8:30 Stop (7 hr in)


### Monday 2021/02/22, 18:00

Editing publisher code...

tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub/build$ cmake ..
tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub/build$ make clean && make
T1:
tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub/build$ ./DDSHelloWorldPublisher
T2:
tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub/build$ ./DDSHelloWorldSubscriber

Finished the first task: publishing and printing a custom message.

Finished the publishTimestamp() method sending the current timestamp in milliseconds as string, need to do the subscribe method.

### 19:30 Stop (8:30 hr in, 1:30 hr coding)

### 20:45
There are many ways to handle the timestamp management for the latency, but because of the low amount of time I decided to use the string "timestamp" as indicator that the message published contains a timestamp for the latency calculation.

Ok! Looks like the latency is working (almost always 0, sometimes 1 ms)

### 21:30 Stop (9:15 hr in, 2:15 hr coding)

### Tuesday 2021/02/23, 07:15

Creating git repo (I should have done earlier).
tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub$ git config --global user.name "Tommaso Martin"
tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub$ git config --global user.email "tommaso.martin555@gmail.com"
tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub$ git init
tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub$ git remote add origin https://gitlab.com/TommasoM/iit_dls_pubsub.git
tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub$ git add .
tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub$ git commit -m "Initial commit, should have done earlier, most tasks are already done"
tom@ubuntu:~/Desktop/IITassignement/iit_pub_sub$ git push -u origin master

Tried multiple topics but failed, too low on time.
Restored previous version with only one topic, next step: Try stress task with only 1 topic.

### 8:10 Stop (10:10 hr in, 3:10 hr coding)

### 13:15

Project refactoring.
Stress test added.

### 14:05 Stop (11:00 hr in, 4:00 hr coding)

























