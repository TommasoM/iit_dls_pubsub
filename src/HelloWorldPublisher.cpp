// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @file HelloWorldPublisher.cpp
 *
 */

#include<iostream>
#include<fstream>
#include<chrono>
// Added these include because not already included in HelloWorldPubSubTypes.h

#include "HelloWorldPubSubTypes.h"

#include <fastdds/dds/domain/DomainParticipantFactory.hpp>
#include <fastdds/dds/domain/DomainParticipant.hpp>
#include <fastdds/dds/topic/TypeSupport.hpp>
#include <fastdds/dds/publisher/Publisher.hpp>
#include <fastdds/dds/publisher/DataWriter.hpp>
#include <fastdds/dds/publisher/DataWriterListener.hpp>

using namespace eprosima::fastdds::dds;

class HelloWorldPublisher
{
private:

    HelloWorld message_;

    DomainParticipant* participant_;

    Publisher* publisher_;

    Topic* topic_;

    DataWriter* writer_;

    TypeSupport type_;

    class PubListener : public DataWriterListener
    {
    public:

        PubListener()
            : matched_(0)
        {
        }

        ~PubListener() override
        {
        }

        void on_publication_matched(
                DataWriter*,
                const PublicationMatchedStatus& info) override
        {
            if (info.current_count_change == 1)
            {
                matched_ = info.total_count;
                std::cout << "Publisher matched." << std::endl;
            }
            else if (info.current_count_change == -1)
            {
                matched_ = info.total_count;
                std::cout << "Publisher unmatched." << std::endl;
            }
            else
            {
                std::cout << info.current_count_change
                        << " is not a valid value for PublicationMatchedStatus current count change." << std::endl;
            }
        }

        std::atomic_int matched_;

    } listener_;

public:

    HelloWorldPublisher()
        : participant_(nullptr)
        , publisher_(nullptr)
        , topic_(nullptr)
        , writer_(nullptr)
        , type_(new HelloWorldPubSubType())
    {
    }

    virtual ~HelloWorldPublisher()
    {
        if (writer_ != nullptr)
        {
            publisher_->delete_datawriter(writer_);
        }
        if (publisher_ != nullptr)
        {
            participant_->delete_publisher(publisher_);
        }
        if (topic_ != nullptr)
        {
            participant_->delete_topic(topic_);
        }
        DomainParticipantFactory::get_instance()->delete_participant(participant_);
    }

    //!Initialize the publisher
    bool init()
    {
        message_.index(0);
	std::ifstream messageFile("message.txt");
	if (messageFile) 
	{
 	    message_.message().assign((std::istreambuf_iterator<char>(messageFile)), std::istreambuf_iterator<char>());
	} else
	{
	    message_.message("ERROR! Could not open message.txt file");
	}
        

        DomainParticipantQos participantQos;
        participantQos.name("Participant_publisher");
        participant_ = DomainParticipantFactory::get_instance()->create_participant(0, participantQos);

        if (participant_ == nullptr)
        {
            return false;
        }

        // Register the Type
        type_.register_type(participant_);

        // Create the publications Topic
        topic_ = participant_->create_topic("HelloWorldTopic", "HelloWorld", TOPIC_QOS_DEFAULT);

        if (topic_ == nullptr)
        {
            return false;
        }

        // Create the Publisher
        publisher_ = participant_->create_publisher(PUBLISHER_QOS_DEFAULT, nullptr);

        if (publisher_ == nullptr)
        {
            return false;
        }

        // Create the DataWriter
        writer_ = publisher_->create_datawriter(topic_, DATAWRITER_QOS_DEFAULT, &listener_);

        if (writer_ == nullptr)
        {
            return false;
        }
        return true;
    }

    //!Send a publication
    bool publish()
    {
        if (listener_.matched_ > 0)
        {
	    std::ifstream messageFile("../message.txt");
	    if (messageFile) 
	    {
 		message_.message().assign((std::istreambuf_iterator<char>(messageFile)), std::istreambuf_iterator<char>());
	    } else
	    {
		message_.message("ERROR! Could not open message.txt file");
	    }
            message_.index(message_.index() + 1);
            writer_->write(&message_);
            return true;
        }
        return false;
    }

    bool stressTest()
    {

        using namespace std::chrono;

	int n_messages = 0;
	std::cout << "\n\nHow many messages you want to publish to stress the system? ";
	std::cin >> n_messages;
	// Get time before publishing
	milliseconds t1 = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
	if (listener_.matched_ > 0)
        {
	    message_.message().assign("stressTestStart");
	    message_.index(message_.index() + 1);
            writer_->write(&message_);
	    message_.message().assign("stressTest generic message");
	    // This for loop sends a starting message, n_messages-2 equal messages and the last one is a "terminator".
	    // A better practice would be to define static global variables for these strings.
	    for (int i=1; i<n_messages-1; i++)
	    {
		message_.index(message_.index() + 1);
        	writer_->write(&message_);
	    }
	    // With the last message I send the STARTING time of the stresstest.
	    // It can be baiasing, but it's more practical this way instead of sending with the stressTestStart.
	    message_.message().assign("stressTestEnd " + std::to_string(t1.count()));
	    message_.index(message_.index() + 1);
            writer_->write(&message_);
            return true;
        }
        return false;
    }

    //!Publish a timestamp to check latency
    bool publishTimestamp()
    {

	using namespace std::chrono;

	milliseconds t1 = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
        if (listener_.matched_ > 0)
        {
            message_.index(message_.index() + 1);
	    message_.message().assign("timestamp " + std::to_string(t1.count()));
            writer_->write(&message_);
            return true;
        }
        return false;
    }

    int menu()
    {
	int choice = 0;
	std::cout << "\n\n### Publisher menu ###";
	std::cout << "\n0) Exit";
	std::cout << "\n1) Publish message.txt once";
	std::cout << "\n2) Check latency between publish and receive time";
	std::cout << "\n3) Stress test";
	std::cout << "\nWhat is your choice? ";
	std::cin >> choice;
	return choice;
    }

    //!Run the Publisher
    void run()
    {
        int choice;
        do 
        {
	    choice = menu();
            switch(choice)
            {
                case 0:
		    break;
		case 1:
		    if (publish())
		    {
			std::cout << "\n Message with index " << message_.index() << " sent";
		    } else
		    {
			std::cout << "\n WARNING: Couldn't send message with index " << message_.index();
		    }
		    break;
		case 2:
		    if (publishTimestamp())
		    {
			std::cout << "\n Message with index " << message_.index() << " sent";
		    } else
		    {
			std::cout << "\n WARNING: Couldn't send message with index " << message_.index();
		    }
		    break;
		case 3:
		    if (stressTest())
		    {
			std::cout << "\n Message with index " << message_.index() << " sent";
		    } else
		    {
			std::cout << "\n WARNING: Couldn't send message with index " << message_.index();
		    }
		    break;
		default:
		    break;
            }
            
        } while (choice != 0);
    }
};

int main(
        int argc,
        char** argv)
{
    std::cout << "Starting publisher." << std::endl;

    HelloWorldPublisher* mypub = new HelloWorldPublisher();
    if(mypub->init())
    {
        mypub->run();
    }

    delete mypub;
    return 0;
}
