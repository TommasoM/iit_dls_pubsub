// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * @file HelloWorldSubscriber.cpp
 *
 */

#include<chrono>

#include "HelloWorldPubSubTypes.h"

#include <fastdds/dds/domain/DomainParticipantFactory.hpp>
#include <fastdds/dds/domain/DomainParticipant.hpp>
#include <fastdds/dds/topic/TypeSupport.hpp>
#include <fastdds/dds/subscriber/Subscriber.hpp>
#include <fastdds/dds/subscriber/DataReader.hpp>
#include <fastdds/dds/subscriber/DataReaderListener.hpp>
#include <fastdds/dds/subscriber/qos/DataReaderQos.hpp>
#include <fastdds/dds/subscriber/SampleInfo.hpp>

using namespace eprosima::fastdds::dds;

int n_messages_;

class HelloWorldSubscriber
{
private:

    DomainParticipant* participant_;

    Subscriber* subscriber_;

    DataReader* reader_;

    Topic* topic_;

    TypeSupport type_;

    class SubListener : public DataReaderListener
    {
    public:

        SubListener()
            : samples_(0)
        {
        }

        ~SubListener() override
        {
        }

        void on_subscription_matched(
                DataReader*,
                const SubscriptionMatchedStatus& info) override
        {
            if (info.current_count_change == 1)
            {
                std::cout << "Subscriber matched." << std::endl;
            }
            else if (info.current_count_change == -1)
            {
                std::cout << "Subscriber unmatched." << std::endl;
            }
            else
            {
                std::cout << info.current_count_change
                        << " is not a valid value for SubscriptionMatchedStatus current count change" << std::endl;
            }
        }

        void on_data_available(
                DataReader* reader) override
        {
	    using namespace std::chrono;
            SampleInfo info;
            if (reader->take_next_sample(&hello_, &info) == ReturnCode_t::RETCODE_OK)
            {
                if (info.valid_data)
                {
                    samples_++;
		    if (hello_.message().find("timestamp") == 0)
		    {
			std::string timestamp;
			timestamp.assign(hello_.message());
			// Latency calculation
			milliseconds t2 = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
			// Search for the first " " and remove what's before that, which should be "timestamp"
			std::string delimiter = " ";
			int pos = timestamp.find(delimiter);
			timestamp.erase(0, pos + delimiter.length());
			// Now just the timestamp as string remains
			long t1_long = std::stol(timestamp);
			std::cout << "\nt1: " << t1_long << std::endl;
			long t2_long = t2.count();
			std::cout << "\nt2: " << t2_long << std::endl;
			long latency = t2_long - t1_long;
			std::cout << "\nDelay pub - sub: " << latency << std::endl;

		    } else if (hello_.message().find("stressTest") == 0)
		    {
			if (hello_.message().find("stressTestStart") == 0)
			{
			    n_messages_=1;
			    std::cout << "\nStarting a stress test\n";
			} else if (hello_.message().find("stressTestEnd") == 0)
			{
			    std::string timestamp;
			    timestamp.assign(hello_.message());
			    // Search for the first " " and remove what's before that, which should be "timestamp"
			    std::string delimiter = " ";
			    int pos = timestamp.find(delimiter);
			    timestamp.erase(0, pos + delimiter.length());
			    // Now just the timestamp as string remains
			    long t1_long = std::stol(timestamp);
			    // Latency calculation
			    milliseconds t2 = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
			    long t2_long = t2.count();
			    long duration = t2_long - t1_long;
			    std::cout << "\nSent " << n_messages_ << " messages in " << duration << "ms";
			    double rate = n_messages_/duration;
			    std::cout << "\nMessage rate: " << rate << " [messages/ms]\n";
			}
			n_messages_++;

		    } else
		    {
                	std::cout << "Message: " << hello_.message() << " with index: " << hello_.index() << " RECEIVED." << std::endl;
		    }
                }
            }
        }

        HelloWorld hello_;

        std::atomic_int samples_;

    } listener_;

public:

    HelloWorldSubscriber()
        : participant_(nullptr)
        , subscriber_(nullptr)
        , topic_(nullptr)
        , reader_(nullptr)
        , type_(new HelloWorldPubSubType())
    {
    }

    virtual ~HelloWorldSubscriber()
    {
        if (reader_ != nullptr)
        {
            subscriber_->delete_datareader(reader_);
        }
        if (topic_ != nullptr)
        {
            participant_->delete_topic(topic_);
        }
        if (subscriber_ != nullptr)
        {
            participant_->delete_subscriber(subscriber_);
        }
        DomainParticipantFactory::get_instance()->delete_participant(participant_);
    }

    //!Initialize the subscriber
    bool init()
    {
        DomainParticipantQos participantQos;
        participantQos.name("Participant_subscriber");
        participant_ = DomainParticipantFactory::get_instance()->create_participant(0, participantQos);

        if (participant_ == nullptr)
        {
            return false;
        }

        // Register the Type
        type_.register_type(participant_);

        // Create the subscriptions Topic
        topic_ = participant_->create_topic("HelloWorldTopic", "HelloWorld", TOPIC_QOS_DEFAULT);

        if (topic_ == nullptr)
        {
            return false;
        }

        // Create the Subscriber
        subscriber_ = participant_->create_subscriber(SUBSCRIBER_QOS_DEFAULT, nullptr);

        if (subscriber_ == nullptr)
        {
            return false;
        }

        // Create the DataReader
        reader_ = subscriber_->create_datareader(topic_, DATAREADER_QOS_DEFAULT, &listener_);

        if (reader_ == nullptr)
        {
            return false;
        }

        return true;
    }

    //!Run the Subscriber
    void run()
    {
        while(true)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }
    }
};

int main(
        int argc,
        char** argv)
{
    std::cout << "Starting subscriber." << std::endl;
    
    HelloWorldSubscriber* mysub = new HelloWorldSubscriber();

    if(mysub->init())
    {
        mysub->run();
    }

    delete mysub;
    return 0;
}
